﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BopDoctorsContactForms.Models.Entidades;
namespace BopDoctorsContactForms.Models.Metodos
{
    public class MetodoEmpresas
    {
        private DBDOCTORS dbcontext;

        public List<Companies> ListadoEmpresa(string file)
        {
            dbcontext = new DBDOCTORS(file);
            return dbcontext.Lista<Companies>();
        }

        public Companies Empresa(string file, int id)
        {
            dbcontext = new DBDOCTORS(file);
            Companies companie = (from e in dbcontext.Lista<Companies>()
                                  where e.id == id
                                  select e).FirstOrDefault();
            return companie;
        }

    }
}
