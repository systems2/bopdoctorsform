﻿using BopDoctorsContactForms.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BopDoctorsContactForms.Models.Metodos
{
    public class MetodoPaises
    {
        private DBDOCTORS dbcontext;

        public List<Countries> listPaises(string file)
        {
            dbcontext = new DBDOCTORS(file);
            return dbcontext.Lista<Countries>();
        }
    }
}
