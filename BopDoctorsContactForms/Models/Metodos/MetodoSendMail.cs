﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using BopDoctorsContactForms.Models.Entidades;
using System.Text;

namespace BopDoctorsContactForms.Models.Metodos
{
    public class MetodoSendMail
    {
        public string Send(persons per, ConfigMail config)
        {
            MailAddress de = new MailAddress(config.mail, config.display);
            MailMessage message = new MailMessage();
            message.From = de;
            message.To.Add(per.mail);
            message.Subject = config.asunto;
            message.BodyEncoding = Encoding.UTF8;
            message.Body = $"{config.mensaje} <a href='{config.enlace}idp={per.id}' target='_black'>Entrar</a>";
            message.IsBodyHtml = true;
            message.Priority = MailPriority.High;
            SmtpClient server = new SmtpClient(config.servidor,config.puerto);
            server.Credentials = new NetworkCredential(config.mail, config.psw);
            server.EnableSsl = config.ssl;
            server.Send(message);
            return "enviado";
        }

        public string prueba()
        {
            MailAddress de = new MailAddress("autorizacionviajes@gloria.com.co", "yo");
            MailMessage message = new MailMessage();
            message.From = de;
            message.To.Add("jerm.pcd@gmail.com");
            message.Subject = "algo";
            message.BodyEncoding = Encoding.UTF8;
            message.Body = "envio";
            message.IsBodyHtml = true;
            message.Priority = MailPriority.High;
            SmtpClient server = new SmtpClient("smtp.office365.com", 587);
            server.Credentials = new NetworkCredential("autorizacionviajes@gloria.com.co", "inicio01");
            server.EnableSsl = true;
            server.Send(message);
            return "enviado";
        }
    }
}
