﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BopDoctorsContactForms.Models.Entidades;

namespace BopDoctorsContactForms.Models.Metodos
{
    public class MetodoControles
    {
        private DBDOCTORS dbcontext;
       
        public bool Guardar(Archivos arc)
        {
            dbcontext = new DBDOCTORS();
            dbcontext.Escribir(arc);
            return dbcontext.Existencia(arc);
        }

        public List<Control> ListaControl(string file)
        {
            dbcontext = new DBDOCTORS(file);
            return dbcontext.Lista<Control>();
        }
    }
}
