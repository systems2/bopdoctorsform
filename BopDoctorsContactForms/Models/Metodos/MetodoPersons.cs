﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BopDoctorsContactForms.Models.Entidades;
using Newtonsoft.Json;
using System.IO;
namespace BopDoctorsContactForms.Models.Metodos
{
    public class MetodoPersons
    {
        private DBDOCTORS dbcontext;
        public bool guardar(Archivos doc, persons persona)
        {
            if (File.Exists(doc.ruta))
            {
                dbcontext = new DBDOCTORS(doc.ruta);
                List<persons> personas = dbcontext.Lista<persons>();
                foreach(persons pe in personas)
                {
                    if (pe.id == persona.id)
                    {
                        personas.Remove(persona);
                    }
                }

                int count = (from pe in personas
                             where pe.id == persona.id
                             select pe).Count();
                if (count == 0)
                {
                    var ruta = doc.ruta.Replace("Persons.json", "ConfigMail.json");
                    ConfigMail mail = (from conf in dbcontext.Lista<ConfigMail>(ruta)
                                       where conf.idpais == persona.pais && conf.idempresa == persona.empresa
                                       select conf
                                                ).FirstOrDefault();
                    MetodoSendMail mts = new MetodoSendMail();
                    mts.Send(persona, mail);
                }

                personas.Add(persona);
                string p = JsonConvert.SerializeObject(personas);
                doc.datos = p;
                dbcontext.Escribir(doc);
                
            }
            else
            {
                dbcontext = new DBDOCTORS();
                dbcontext.Escribir(doc);
                var ruta = doc.ruta.Replace("Persons.json", "ConfigMail.json");
                ConfigMail mail = (from conf in dbcontext.Lista<ConfigMail>(ruta)
                                   where conf.idpais == persona.pais && conf.idempresa == persona.empresa
                                   select conf
                                            ).FirstOrDefault();
                MetodoSendMail mts = new MetodoSendMail();
                mts.Send(persona, mail);
            }
            return dbcontext.Existencia(doc);
        }

        public List<persons> ListaPersona(string doc, Control control)
        {
            dbcontext = new DBDOCTORS(doc);
            var listp = (from p in dbcontext.Lista<persons>()
                         where p.empresa == control.idempresa && p.pais == control.idpais
                         select p).ToList();
            return listp;
        }

        public persons Persona(string doc, Control control)
        {

            if (File.Exists(doc))
            {
                dbcontext = new DBDOCTORS(doc);
                var per = (from p in dbcontext.Lista<persons>()
                           where p.id == control.nomc
                           select p).FirstOrDefault();
                if (per == null)
                {
                    var newdoc = doc.Replace($"{control.idpais}_{control.idempresa}", "");
                    dbcontext = new DBDOCTORS(newdoc);
                    per = (from p in dbcontext.Lista<persons>()
                               where p.id == control.nomc
                               select p).FirstOrDefault();
                }
                return per;
            }
            else
            {
                var newdoc = doc.Replace($"{control.idpais}_{control.idempresa}", "");
                dbcontext = new DBDOCTORS(newdoc);
                var per = (from p in dbcontext.Lista<persons>()
                           where p.id == control.nomc
                           select p).FirstOrDefault();
                return per;
            }

        }

        public void Filesave(string r, string f)
        {
            dbcontext = new DBDOCTORS();
            dbcontext.GuardarFile(r,f);
        }
    }
}
