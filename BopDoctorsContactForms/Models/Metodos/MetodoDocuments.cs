﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BopDoctorsContactForms.Models.Entidades;
using Newtonsoft.Json;

namespace BopDoctorsContactForms.Models.Metodos
{
    public class MetodoDocuments
    {
        private DBDOCTORS dbcontext;

        public bool Guardar(Archivos arc, Documents doc)
        {
            if (File.Exists(arc.ruta))
            {
                dbcontext = new DBDOCTORS(arc.ruta);
                List<Documents> documentos = dbcontext.Lista<Documents>();
                documentos.Add(doc);
                string d = JsonConvert.SerializeObject(documentos);
                arc.datos = d;
                dbcontext.Escribir(arc);
            }
            else
            {
                List<Documents> docs = new List<Documents>();
                docs.Add(doc);
                arc.datos= JsonConvert.SerializeObject(docs);
                dbcontext = new DBDOCTORS();
                dbcontext.Escribir(arc);
            }

            return dbcontext.Existencia(arc);
        }

        public List<Documents> ListaoDoc(string doc, Control control)
        {
            dbcontext = new DBDOCTORS(doc);
            var listp = (from d in dbcontext.Lista<Documents>()
                         where d.idperson == control.nomc
                         select d).ToList();
            return listp;
        }
    }
}
