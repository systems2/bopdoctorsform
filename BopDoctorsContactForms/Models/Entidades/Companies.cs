﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BopDoctorsContactForms.Models.Entidades
{
    public class Companies
    {
        public int id { get; set; }
        public string name { get; set; }
        public string logo { get; set; }
    }
}
