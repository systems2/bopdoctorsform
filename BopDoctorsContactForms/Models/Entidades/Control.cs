﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BopDoctorsContactForms.Models.Entidades
{
    public class Control
    {
        public string id { get; set; }
        public int idpais { get; set; }
        public int idempresa { get; set; }
        public string nomc { get; set; }
        public string tpc { get; set; }
    }
}

