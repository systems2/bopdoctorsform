﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BopDoctorsContactForms.Models.Entidades
{
    public class ConfigMail
    {
        public int idpais { get; set; }
        public int idempresa { get; set; }
        public string display { get; set; }
        public string asunto { get; set; }
        public string enlace { get; set; }
        public string mail { get; set; }
        public string psw { get; set; }
        public bool ssl { get; set; }
        public string servidor { get; set; }
        public int puerto { get; set; }
        public string mensaje { get; set; }
    }
}
