﻿using Microsoft.AspNetCore.Http;
using System.IO;

namespace BopDoctorsContactForms.Models.Entidades
{
    public interface IFormFile
    {
        string ContentType { get; }
        string ContentDisposition { get; }
        IHeaderDictionary Headers { get; }
        long Length { get; }
        string Name { get; }
        string FileName { get; }
        Stream OpenReadStream();
        void CopyTo(Stream target);
    }
}
