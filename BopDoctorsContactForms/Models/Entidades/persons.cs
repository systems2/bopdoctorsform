﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BopDoctorsContactForms.Models.Entidades
{
    public class persons
    {
        public string id { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public int pais { get; set; }
        public long celular { get; set; }
        public int empresa { get; set; }
        public string afiliado { get; set; }
        public string mail { get; set; }
        public string usuario { get; set; }
        public string psw { get; set; }
        public List<NewControl> newcontrol { get; set; }
    }
}
