﻿
classDatos = new AccesosDatos();
classDatos.Ajax("/Home/ListaPaises", '', function (datos) {
    $("#cmbpais").html("").append("<option value='-1'>Seleccione</option>");
    for (var x in datos) {
        $("#cmbpais").append("<option value='" + datos[x].idpais + "'>" + datos[x].Name + "</option>")
    }
})

classDatos.Ajax("/Home/ListaEmpresa", '', function (datos) {
    $("#cmbempresa").html("").append("<option value='-1'>Seleccione</option>");
    for (var x in datos) {
        $("#cmbempresa").append("<option value='" + datos[x].id + "'>" + datos[x].name + "</option>")
    }
})

$("body").on("click", "#btnsave", function () {
    var person =   {
        "nombres": $("#txtnom").val(),
        "apellidos": $("#txtapel").val(),
        "pais": $("#cmbpais").val(),
        "celular": $("#txtmovil").val(),
        "empresa": $("#cmbempresa").val(),
        "afiliado": $("#txtafiliado").val(),
        "mail": $("#txtmail").val()
    };
    classDatos.Ajax(
        "/Home/Guardar",
        JSON.stringify(person),
        function () {
            classDatos.LimpiarCampos(".form-control");
            classDatos.CloseWindows("#div-confirmacion");
        }
    )
})

$("body").on("click", "#btnconfirm", function () {
    if ($("#txtnom").val() != "" && $("#txtapel").val() != "" && $("#txtafiliado").val() != "" && $("#txtmovil").val() != ""
        && $("#txtmail").val() != "" && $("#cmbpais").val() != "-1" && $("#cmbempresa").val() != "-1") {
        classDatos.OpenWindows("#div-confirmacion", "Confirmación", 100, 300);
    } else {
        classDatos.OpenWindows("#div-alert", "Advertencia", 100, 300);
    }    
})

$("body").on("click", "#btnclose", function () {
    classDatos.CloseWindows("#div-alert")
})

$("body").on("click", "#btncancel", function () {
    classDatos.CloseWindows("#div-confirmacion")
})

$("body").on("click", "#btnclean", function () {
    classDatos.LimpiarCampos(".form-control");
})
