﻿classDatos = new AccesosDatos();
var control;

classDatos.Ajax("/Home/ListaPaises", '', function (datos) {
    $("#cmbpais").html("").append("<option value='-1'>Seleccione</option>");
    for (var x in datos) {
        $("#cmbpais").append("<option value='" + datos[x].idpais + "'>" + datos[x].Name + "</option>")
    }
})

classDatos.Ajax("/Home/ListaEmpresa", '', function (datos) {
    $("#cmbempresa").html("").append("<option value='-1'>Seleccione</option>");
    for (var x in datos) {
        $("#cmbempresa").append("<option value='" + datos[x].id + "'>" + datos[x].name + "</option>")
    }
})

$("body").on("change", "#cmbempresa", function () {
    empresa = {
        id: $(this).val()
    }
    classDatos.Ajax(
        "/Home/Empresa",
        JSON.stringify(empresa),
        function (datos) {
            $("#imgemp").attr("src", datos.logo);
            $("#lblemp").text(datos.name)
        })
})

var myarray = new Array;
var i = 0;
$("body").on("click", "#btnadd", function () {
    if ($("#cmbtpc").val() != "" && $("#txtnomc").val() != "") {
        idc = "div_" + i;
        var control = {
            id: idc,
            idpais: $("#cmbpais").val(),
            idempresa: $("#cmbempresa").val(),
            nomc: $("#txtnomc").val().toUpperCase(),
            tpc: $("#cmbtpc").val()
        };
        $("#divadd").append(
            '<div id="' + control.id + '" class="col-md-6" > ' +
            '<div class="input-group mb-3">' +
            '<div class="input-group-prepend">' +
            '<span class="input-group-text">' + control.nomc + ' :</span>' +
            '</div>' +
            '<input type="' + control.tcp + '" class="form-control"/>' +
            '</div>' +
            '</div>'
        );
        myarray.push(control);
        classDatos.Grilla(
            "#div-grid-controles",
            myarray,
            [
                {
                    field: "tpc",
                    title: "Tipo de Control"
                },
                {
                    field: "nomc",
                    title: "Nombre"
                },
                {
                    field: "id",
                    title: "Quitar",
                    template: function (d) {
                        return '<img del="' + d.id + '" style="max-width=2em; max-height: 3em;" src="../images/cancel.ico" />'
                    }
                }
            ]
        )
        if ($("#cmbtpc").val() == "date") {
            $("[type='date']").kendoDatePicker({
                format: "yyyy-MM-dd"
            });
        }
        $("#txtnomc").val("")
        $("#cmbtpc").val("-1")
        i++;
    } else {
        classDatos.OpenWindows("#div-alert", "Advertencia", 100, 300);
    }


})

$("body").on("click", "#btnclose", function () {
    classDatos.CloseWindows("#div-alert")
})

$("body").on("click", "#btnquitar", function () {
    id = $("#lbldiv").val();
    for (x in myarray) {
        if (myarray[x].id == id) {
            myarray.splice(x, 1)
            $("#" + id).remove();
        }
    }
    $("#div-grid-controles").data('kendoGrid').dataSource.read();
    $("#div-grid-controles").data("kendoGrid").refresh();
    classDatos.CloseWindows("#div-confirmacion-quitar");
})

$("body").on("click", "#btncancelquitar", function () {
    classDatos.CloseWindows("#div-confirmacion-quitar");
})

$("body").on("click", "[del]", function () {
    $("#lbldiv").val($(this).attr("del"));
    classDatos.OpenWindows("#div-confirmacion-quitar", "Quitar Campo", 100, 300)
})

$("body").on("click", "#btnclean", function () {
    $("#txtnomc").val("");
    $("#cmbtpc").val("-1");
})

var ini = 0;
$("body").on("click", "#v-pills-profile-tab", function () {
    if (ini == 0) {
        classDatos.Upload("#fileDoc", " ", "Subir Documentos")
    }
    ini++;
})

$("body").on("click", "#btnconfirm", function () {
    validarcampo = classDatos.ValidarCampos("#div-empresa-pais .form-control")   
    if (validarcampo && myarray.length>0) {
        classDatos.OpenWindows("#div-confirmacion", "Advertencia", 100, 300);
    } else {
        classDatos.OpenWindows("#div-alert", "Advertencia", 100, 300);
    }
})

$("body").on("click", "#btnsave", function () { 
    controles = myarray;
    classDatos.Ajax(
        "/Home/GuardarControl",
        JSON.stringify(controles),
        function (datos) {
            classDatos.LimpiarCampos("#v-pills-tabContent .form-control")
            myarray.splice(0, myarray.length)
            classDatos.CloseWindows("#div-confirmacion");
            $("#div-grid-controles").data('kendoGrid').dataSource.read();
            $("#div-grid-controles").data("kendoGrid").refresh();
        })
})
$("body").on("click", "#btncancel", function () {
    classDatos.CloseWindows("#div-confirmacion")
})



