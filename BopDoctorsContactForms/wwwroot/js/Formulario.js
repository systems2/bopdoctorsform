﻿classDatos = new AccesosDatos();

control = {
    idpais: $("#lblpais").val(),
    idempresa: $("#lblempresa").val(),
};
classDatos.Ajax(
    "/Home/ListaPacientes",
    JSON.stringify(control),
    function (datos) {
        classDatos.Grilla(
            "#div-grid-clientes",
            datos,
            [
                {
                    field: "nombres",
                    title: "Nombres"
                },
                {
                    field: "apellidos",
                    title: "Apellidos"
                },
                {
                    field: "celular",
                    title: "Celular"
                },
                {
                    field: "mail",
                    title: "mail"
                },
                {
                    field: "celular",
                    title: "Editar",
                    template: function (d) {
                        return '<img  edit="' + d.id + '" style="max-width=2em; max-height: 3em;" src="../../images/edit.ico" />'
                    }
                }
            ]
        )
    })

$("body").on("click", "[edit]", function () {
    window.open('../FormClient/?idpais=' + $("#lblpais").val() + '&idempresa=' + $("#lblempresa").val() + '&idp=' + $(this).attr("edit"));
})







