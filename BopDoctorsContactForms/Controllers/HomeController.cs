﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BopDoctorsContactForms.Models;
using BopDoctorsContactForms.Models.Metodos;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Hosting;
using BopDoctorsContactForms.Models.Entidades;
using System.IO;

namespace BopDoctorsContactForms.Controllers
{
    public class HomeController : Controller
    {

        private string file;
        private string ruta;
        public HomeController(IHostingEnvironment env)
        {
            IHostingEnvironment _env = env;
            var webroot = _env.WebRootPath;
            ruta = $"{ webroot}\\JSON";
        }


        [HttpGet]
        public string Send()
        {
            MetodoSendMail mtd = new MetodoSendMail();
            return mtd.prueba();
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Creacion()
        {
            return View("CreationForm");
        }

        [Route("Formulario/FormAdmin/{*id}")]
        public IActionResult Form(int idpais, int idempresa)
        {
            Control c = new Control();
            c.idpais = idpais;
            c.idempresa = idempresa;
            return View("Formulario", c);
        }


        [Route("Formulario/FormClient/{*id}")]
        public IActionResult FormCliente(int idpais, int idempresa, string idp)
        {
            Control c = new Control();
            c.idpais = idpais;
            c.idempresa = idempresa;
            c.nomc = idp;
            return View("FormClient", c);
        }

        [HttpPost]
        public string ListaPaises()
        {
            file = System.IO.Path.Combine(ruta, "Countries.json");
            MetodoPaises paises = new MetodoPaises();
            String listpaises = JsonConvert.SerializeObject(paises.listPaises(file));
            return listpaises;
        }

        [HttpPost]
        public string ListaEmpresa()
        {
            file = System.IO.Path.Combine(ruta, "Companies.json");
            MetodoEmpresas empresas = new MetodoEmpresas();
            string listempresa = JsonConvert.SerializeObject(empresas.ListadoEmpresa(file));
            return listempresa;
        }

        [HttpPost]
        public string Empresa([FromBody] Companies emp)
        {
            file = System.IO.Path.Combine(ruta, "Companies.json");
            MetodoEmpresas empresas = new MetodoEmpresas();
            string listempresa = JsonConvert.SerializeObject(empresas.Empresa(file, emp.id));
            return listempresa;
        }

        [HttpPost]
        public bool Guardar([FromBody] persons person)
        {
            person.id = Guid.NewGuid().ToString();
            file = System.IO.Path.Combine(ruta, "Persons.json");
            Archivos arc = new Archivos();
            List<persons> per = new List<persons>();
            per.Add(person);
            string _per = JsonConvert.SerializeObject(per);
            arc.datos = _per;
            arc.ruta = file;
            MetodoPersons mtper = new MetodoPersons();
            return mtper.guardar(arc, person);
        }

        [HttpPost]
        public bool GuardarControl([FromBody] List<Control> controles)
        {
            var emp = controles.Select(c => new { idemp = c.idempresa, idpais = c.idpais }).FirstOrDefault();
            file = System.IO.Path.Combine(ruta, $"{emp.idemp}_{emp.idpais}Controles.json");
            Archivos arc = new Archivos();
            string _control = JsonConvert.SerializeObject(controles);
            arc.datos = _control;
            arc.ruta = file;
            MetodoControles mtcontrol = new MetodoControles();
            return mtcontrol.Guardar(arc);
        }

        [HttpPost]
        public string ListaControl([FromBody] Control control)
        {
            file = System.IO.Path.Combine(ruta, $"{control.idpais}_{control.idempresa}Controles.json");
            MetodoControles controles = new MetodoControles();
            string listempresa = JsonConvert.SerializeObject(controles.ListaControl(file));
            return listempresa;
        }

        [HttpPost]
        public string ListaPacientes([FromBody] Control control)
        {

            file = System.IO.Path.Combine(ruta, "Persons.json");
            MetodoPersons persons = new MetodoPersons();
            string Listpersons = JsonConvert.SerializeObject(persons.ListaPersona(file, control));
            return Listpersons;
        }

        [HttpPost]
        public string Cliente([FromBody] Control control)
        {

            file = System.IO.Path.Combine(ruta, $"{control.idpais}_{control.idempresa}Persons.json");
            MetodoPersons persons = new MetodoPersons();
            string person = JsonConvert.SerializeObject(persons.Persona(file, control));
            return person;
        }
        
        [HttpPost("UploadFiles")]
        public string subirArhico()
        {
            persons person = new persons();
            person.id = Request.Form["id"];
            person.nombres = Request.Form["nombres"];
            person.apellidos = Request.Form["apellidos"];
            person.pais = int.Parse(Request.Form["pais"]);
            person.celular = long.Parse(Request.Form["celular"]);
            person.empresa = int.Parse(Request.Form["empresa"]);
            person.afiliado = Request.Form["afiliado"];
            person.mail = Request.Form["mail"];
            person.usuario = Request.Form["usuario"];
            person.psw = Request.Form["psw"];
            var ar = Request.Form["newcontrol"];
            List<NewControl> a = JsonConvert.DeserializeObject<List<NewControl>>(ar[0]);
            person.newcontrol = a;
            file = System.IO.Path.Combine(ruta, $"{person.pais}_{person.empresa}Persons.json");
            Archivos arc = new Archivos();
            List<persons> per = new List<persons>();
            per.Add(person);
            string _per = JsonConvert.SerializeObject(per);
            arc.datos = _per;
            arc.ruta = file;
            MetodoPersons mtper = new MetodoPersons();
            mtper.guardar(arc, person);

            var filedata = Request.Form.Files["Filedata"];

            if (filedata != null)
            {
                Documents doc = new Documents();
                MetodoDocuments mtdoc = new MetodoDocuments();
                doc.idperson = person.id;
                doc.filename = filedata.FileName.Replace(" ", "");
                var r = ruta.Replace("JSON", "Documentos");
                file = System.IO.Path.Combine(ruta, $"{person.pais}_{person.empresa}Documents.json");
                arc.datos=  JsonConvert.SerializeObject(doc);
                arc.ruta = file;
                mtdoc.Guardar(arc, doc);
                file = System.IO.Path.Combine(r, doc.filename);
                using (var stream = new FileStream(file, FileMode.Create))
                {
                    filedata.CopyTo(stream);
                }
            }

            return "guardo";
        }

        [HttpPost]
        public string ListaDocumentos([FromBody] Control control)
        {

            file = System.IO.Path.Combine(ruta, $"{control.idpais}_{control.idempresa}Documents.json");
            MetodoDocuments docs = new MetodoDocuments();
            string Listpersons = JsonConvert.SerializeObject(docs.ListaoDoc(file, control));
            return Listpersons;
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
